import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ChatBoxComponent} from './components/chat-box/chat-box.component';
import {ChatInputComponent} from './components/chat-input/chat-input.component';
import {ChatHeaderComponent} from './components/chat-header/chat-header.component';
import {ChatContentsComponent} from './components/chat-contents/chat-contents.component';
import {HTTP_INTERCEPTORS, HttpClientModule,} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {HttpHelppers} from './utils/http-helppers';


@NgModule({
  declarations: [
    AppComponent,
    ChatBoxComponent,
    ChatInputComponent,
    ChatHeaderComponent,
    ChatContentsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: HttpHelppers, multi: true}, /* https://angular.io/api/common/http/HttpClientXsrfModule */
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
