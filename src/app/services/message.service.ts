import {Injectable} from '@angular/core';
import {Message, MessageAnswer, MessageQuestion} from '../models/message';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {AppConfigs} from '../app.configs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  private subject = new Subject<Message>();

  constructor(private http: HttpClient) {
  }

  getSubscribe() {
    return this.subject.asObservable();
  }

  getMessageHistory() {
    const answer = new MessageAnswer();
    answer.answer = 'Chào bạn! Tôi sẵn sàng tư vấn cho bạn.';
    answer.idMsgLog = -1;
    return [answer];
  }

  queryQuesion(question: string) {
    const que = new MessageQuestion();
    que.idMsgLog = -1;
    que.question = question;
    que.requestTime = '1987-09-03T00:00:00.000Z';
    this.subject.next(que);

    this.http.post(AppConfigs.getApiUrl('/api/request'), que)
      .subscribe((res: any) => {
        console.log('call request /api/request response ::: ', res);
        const answer = new MessageAnswer();
        answer.answer = res.answer;
        answer.idMsgLog = res.id_msg_log;
        this.subject.next(answer);
      });
  }
}
