import {Component, OnDestroy, OnInit} from '@angular/core';
import {Message} from '../../models/message';
import {MessageService} from '../../services/message.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-chat-contents',
  templateUrl: './chat-contents.component.html',
  styleUrls: ['./chat-contents.component.css']
})
export class ChatContentsComponent implements OnInit, OnDestroy {

  messages: Message[];
  subscription: Subscription;

  constructor(private messageService: MessageService) {
    this.messages = this.messageService.getMessageHistory();
  }

  ngOnInit() {
    this.subscription = this.messageService.getSubscribe().subscribe(message => {
      this.messages.push(message);
    });
  }

  ngOnDestroy(): void {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }

}
