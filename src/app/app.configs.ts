export class AppConfigs {
  static getApiUrl(url: string): string;
  static getApiUrl(url: string) {
    if (url && typeof url === 'string') {
      console.log('AppConfigs call', url);
      return 'http://127.0.0.1:8000' + url;
    } else {
      return 'http://127.0.0.1:8000';
    }
  }
}
