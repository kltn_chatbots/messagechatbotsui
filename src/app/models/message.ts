export class Message {
  idMsgLog: number; // -1 == null

  isAnswer() {
    return this instanceof MessageAnswer;
  }
}

export class MessageQuestion extends Message {
  question: string;
  requestTime: string;
}

export class MessageAnswer extends Message {
  answer: string;
}
