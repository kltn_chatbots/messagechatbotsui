import {Component, OnInit} from '@angular/core';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-chat-input',
  templateUrl: './chat-input.component.html',
  styleUrls: ['./chat-input.component.css']
})
export class ChatInputComponent implements OnInit {

  queryMessage: string;

  constructor(private messageService: MessageService) {
  }

  ngOnInit() {
  }

  keyUpMessage() {
    this.messageService.queryQuesion(this.queryMessage);
    this.queryMessage = null;
  }
}
