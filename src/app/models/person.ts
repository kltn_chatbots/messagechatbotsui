export class Person {
  name: string;
  province: string;
  numberPhone: string;
  email: string;
}
